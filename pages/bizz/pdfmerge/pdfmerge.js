var app = getApp();
var api = require('../../../utils/api.js');
Page({
    data: {
        list: [], isIcon: {}, scrollTop: 0,
        motto: 'PDF合并',
        invoice:'bg-blue-thin',
        invoiceIcon:'_icon-add-round',
        invoiceText:'请选择电子发票文件',
        travel:'bg-blue-thin',
        travelIcon:'_icon-add-round',
        travelText:'请选择行程报销单文件',
        apiSource:'作者自研-源码开源',
        result: [],
        fileKeyA:null,
        fileKeyB:null,
        fileA:null,
        fileB:null,
        pdfImage:null,
        images: {},
        resultData: null,
        img: '',
        imgA: '',
        imgB: '',
        modalName: '',
        modalTitle: null,
        pdfURL:null,
        modalContent: null,
        isBanner:true,
        isGray:false,
        garyType:5,
        removeSlogans:1,
        grayDeal:100
    },
    //复制地址
    CopyLink(e) {
        wx.setClipboardData({
            data:this.data.pdfURL,
            success: res => {
                wx.showToast({
                    title: '已复制',
                    duration: 1000,
                })
            }
        })
    },
    /**
     * 更改Banner类型
     */
    bannerChange:function(e){
        var that = this;
        var status = e.detail;
        if(status == '1'){
            that.setData({
                isBanner:true
            })
        }else{
            that.setData({
                isBanner:false
            })
        }
        that.setData({
            removeSlogans:status
        })
    },
    /**
     * 更改灰度类型
     */
    grayChange:function(e){
        var that = this;
        var status = e.detail;
        if(status == '100'){
            that.setData({
                isGray:true
            })
        }else{
            that.setData({
                isGray:false
            })
        }
        that.setData({
            grayDeal:status
        })
    },
    //用户点击右上角分享朋友圈
    onShareTimeline: function () {
    },
    //用户点击右上角分享朋友|朋友圈
    onShareAppMessage: function () {
        wx.showShareMenu({
            withShareTicket: true,
            menus: ['shareAppMessage', 'shareTimeline']
        })
        return {
            title: '电子发票出行报销单合并',
            path: '/pages/bizz/pdfmerge/pdfmerge'
        }
    },
    clear: function (event) {
        wx.clearStorage();
    },
    //事件处理函数
    bindViewTap: function () {
        wx.navigateTo({
            url: '../logs/logs'
        })
    },
    //从聊天页面选文件
    chooseMessageA:function(){
        var that = this;
        wx.chooseMessageFile({
            count: 1,
            type:'file',
            extension:['pdf'],
            success(res){
                console.info(res);
                let fileName = res.tempFiles[0].name;
                let suffix = fileName.substring(fileName.lastIndexOf('.')+1);
                if(suffix=='pdf'){
                    if (res.tempFiles[0].size > (4096 * 1024)) {
                        wx.showToast({
                            title: '文件过大哦',
                            icon: 'none',
                            mask: true,
                            duration: 1500
                        })
                    } else {
                        that.setData({
                            imgA: res.tempFiles[0].path
                        })
                        that.uploadFile(res.tempFiles[0].path,that.data.fileKeyA,fileName);
                    }
                }else{
                    wx.showToast({
                        title: '请选择PDF文件哦',
                        icon: 'none',
                        mask: true,
                        duration: 1500
                    })
                }
            }
        })
    },
    //从聊天页面选文件
    chooseMessageB:function(){
        var that = this;
        wx.chooseMessageFile({
            count: 1,
            type:'file',
            extension:['pdf'],
            success(res){
                let fileName = res.tempFiles[0].name;
                let suffix = fileName.substring(fileName.lastIndexOf('.')+1);
                if(suffix=='pdf'){
                    if (res.tempFiles[0].size > (4096 * 1024)) {
                        wx.showToast({
                            title: '文件过大哦',
                            icon: 'none',
                            mask: true,
                            duration: 1500
                        })
                    } else {
                        that.setData({
                            imgB: res.tempFiles[0].path
                        })
                        that.uploadFile(res.tempFiles[0].path,that.data.fileKeyB,fileName);
                    }
                }else{
                    wx.showToast({
                        title: '请选择PDF文件哦',
                        icon: 'none',
                        mask: true,
                        duration: 1500
                    })
                }
            }
        })
    },
    //上传PDF
    uploadFile(file,key,fileName){
        var that = this;
        var keyStart = key.indexOf("A");
        if(keyStart==0){
            wx.showLoading({
                title: "发票上传",
                mask: true
            })
        }else{
            wx.showLoading({
                title: "行程单上传",
                mask: true
            })
        }
        api.pdfUploadRequest(file,key,app.globalData.userId, {
            success(result) {
                var resultJ = JSON.parse(result)
                wx.hideLoading();
                if (resultJ.code == 200) {
                    if(keyStart==0){
                        that.setData({
                            invoice:'bg-green-thin',
                            invoiceIcon:'_icon-check-round-o',
                            invoiceText:fileName
                        })
                    }else{
                        that.setData({
                            travel:'bg-green-thin',
                            travelIcon:'_icon-check-round-o',
                            travelText:fileName
                        })
                    }
                    if(keyStart==0){
                        that.setData({
                            fileA: resultJ.data_flower.file_key_a
                        })
                    }else{
                        that.setData({
                            fileB: resultJ.data_flower.file_key_b
                        })
                    }
                } else {
                    if (resultJ.code == 87014) {
                        wx.hideLoading();
                        wx.showModal({
                            content: '存在敏感内容，请更换图片',
                            showCancel: false,
                            confirmText: '明白了'
                        })
                        if(keyStart==0){
                            that.setData({
                                imgA: null
                            })
                        }else{
                            that.setData({
                                imgB: null
                            })
                        }
                    } else {
                        wx.hideLoading();
                        wx.showModal({
                            content: resultJ.msg_zh,
                            showCancel: false,
                            confirmText: '明白了'
                        })
                    }
                }
            }
        })
    },
    //进行合并
    uploadMerge:function () {
        var that = this;
        if(null!=that.data.fileA && null!=that.data.fileB ){
            wx.showLoading({
                title: "PDF合并中...",
                mask: true
            })
            api.pdfMergeRequest(app.globalData.userId,that.data.fileA,that.data.fileB,
                that.data.isBanner,that.data.isGray,that.data.garyType, {
                    success(result) {
                        var resultJ = result
                        wx.hideLoading();
                        if (resultJ.code == 200) {
                            that.setData({
                                pdfImage:'data:image/jpg;base64,' + resultJ.data_flower.image_base64,
                                pdfURL:resultJ.data_flower.file_url
                            })
                        } else {
                            if (resultJ.code == 87014) {
                                wx.hideLoading();
                                wx.showModal({
                                    content: '存在敏感内容，请更换文件',
                                    showCancel: false,
                                    confirmText: '明白了'
                                })
                            } else {
                                wx.hideLoading();
                                wx.showModal({
                                    content: resultJ.msg_zh,
                                    showCancel: false,
                                    confirmText: '明白了'
                                })
                            }
                        }
                    }
                })
        } else {
            wx.showModal({
                content: '请确保发票、行程单都已上传',
                showCancel: false,
                confirmText: '好的'
            })
        }
    },
    onLoad: function () {
        var that = this;
        that.setData({
            list: [{icon: '_icon-home-o', num: 1}],
            isIcon: {down: 'cicon-unfold-less', top: 'cicon-eject', up: 'cicon-unfold-more'}
        })
        var timestamp = Date.parse(new Date());
        var random =  app.globalData.userId
        that.setData({
            fileKeyA:'A'+timestamp+random,
            fileKeyB:'B'+timestamp+random
        })
    },
    tapToolsBar(e) {
        var pageNum = e.detail.item.num;
        if(pageNum==1){
            wx.switchTab({
                url: '/pages/index/index',
            })
        }
    },
    /**
     * 重新合并
     */
    againMerge(){
        var that = this;
        var timestamp = Date.parse(new Date());
        var random =  app.globalData.userId
        that.setData({
            fileKeyA:'A'+timestamp+random,
            fileKeyB:'B'+timestamp+random,
            imgA:null,
            imgB:null,
            pdfImage:null,
            pdfURL:null,
            fileA:null,
            fileB:null,
            invoice:'bg-blue-thin',
            invoiceIcon:'_icon-add-round',
            invoiceText:'请选择电子发票文件',
            travel:'bg-blue-thin',
            travelIcon:'_icon-add-round',
            travelText:'请选择行程报销单文件',
        })
    },
    /**
     * 点击查看图片，可以进行保存
     */
    preview(e) {
        var that = this;
        if (null == that.data.pdfImage || that.data.pdfImage == '') {
            wx.showModal({
                title: '温馨提示',
                content: '未选择任何PDF文件',
                showCancel: false,
                confirmText: '好的'
            })
        } else {
            wx.previewImage({
                urls: [that.data.pdfImage],
                current: that.data.pdfImage
            })
        }
    }
});
