const app = getApp();
var api = require('../../utils/api.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        gridLifeList: [
            {
                icon: '../../static/images/ailife/logo.png',
                color: 'red',
                badge: 20,
                id: 11,
                name: 'LOGO识别'
            }]
    },
    //页面跳转
    toPage: function (event) {
        var route = event.currentTarget.id;
        if (route == 11) {
            wx.navigateTo({
                url: '/pages/bizz/logo/logo',
            })
        }

    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})
