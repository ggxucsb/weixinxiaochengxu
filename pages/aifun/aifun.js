const app = getApp();
var api = require('../../utils/api.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        gridFunList: [
            {
                icon: '../../static/images/aifunplay/faceflower.png',
                color: 'red',
                badge: 20,
                id: 11,
                name: '送你一朵小红花'
            }, {
                icon: '../../static/images/aifunplay/cpface.png',
                color: 'red',
                badge: 20,
                id: 12,
                name: '情侣拼脸'
            }, {
                icon: '../../static/images/aifunplay/symmetryFace.png',
                color: 'red',
                badge: 20,
                id: 13,
                name: '对称脸'
            }],
        gridSkinList: [
            {
                icon: '../../static/images/health/skin.png',
                color: 'red',
                badge: 20,
                id: 21,
                name: '肤质分析'
            }]
    },
    //页面跳转
    toPage: function (event) {
        var route = event.currentTarget.id;
        if (route == 11) {
            wx.navigateTo({
                url: '/pages/bizz/faceflower/faceflower',
            })
        }
        if (route == 12) {
            wx.navigateTo({
                url: '/pages/bizz/cpface/cpface',
            })
        }
        if (route == 13) {
            wx.navigateTo({
                url: '/pages/bizz/symmetryface/symmetryface',
            })
        }
        if (route == 21) {
            wx.navigateToMiniProgram({
                appId: 'wx593f6d142ade61c9'
            })
        }

    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})
