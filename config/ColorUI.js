//框架核心配置
import ColorUI from '../mp-cu/main'

export const colorUI = new ColorUI({
    config: {
        theme: 'auto',
        main: 'xsBlue',
        text: 1,
        footer: false,
        share: true,
        shareTitle: '小帅一点资讯',
        homePath: '/pages/index/index',
        tabBar: [
            {
                title: '主页',
                icon: '/static/tab_icon/tpl.png',
                curIcon: '/static/tab_icon/tpl_cur.png',
                url: '/pages/index/index',
                type: 'tab'
            },
            {
                title: '趣玩AI',
                icon: '/static/tab_icon/find.png',
                curIcon: '/static/tab_icon/find_cur.png',
                url: '/pages/aifun/aifun',
                type: 'tab'
            },
            {
                title: '生活AI',
                icon: '/static/tab_icon/custom.png',
                curIcon: '/static/tab_icon/custom_cur.png',
                url: '/pages/ailife/ailife',
                type: 'tab'
            },
            {
                title: 'LabAI',
                icon: '/static/tab_icon/test.png',
                curIcon: '/static/tab_icon/test_cur.png',
                url: '/pages/ailab/ailab',
                type: 'tab'
            },
            {
                title: '我的',
                icon: '/static/tab_icon/my.png',
                curIcon: '/static/tab_icon/my_cur.png',
                url: '/pages/mine/mine',
                type: 'tab'
            }
        ],
    }
})
